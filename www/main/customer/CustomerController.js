/**
 * Created by mhill168 on 07/09/15.
 */

(function () {

    'use strict';

    angular.module('footFall')
        .controller('CustomerController', CustomerController);

    CustomerController.$inject = ['$scope', '$rootScope', 'localStorageService', '$timeout'];

    function CustomerController($scope, $rootScope, localStorageService, $timeout) {

      $scope.viewTitle = "Customer";

      // listen for the $ionicView.enter event:
      $scope.$on('$ionicView.enter', function(e) {
          $scope.callStorage();
      });

      $scope.callStorage = function () {
        var inStore = localStorageService.get('customers');
        $rootScope.customers = inStore || [];

        $scope.$watch('customers', function () {
          localStorageService.set('customers', $rootScope.customers);
        }, true);

      };

    }

})();
