/**
 * Created by mhill168 on 07/09/15.
 */

(function () {

    'use strict';

    angular.module('footFall')
        .controller('UserService', UserService);

    UserService.$inject = ['$filter', '$q', 'localStorageService', '$timeout'];

    function UserService($filter, $q, localStorageService, $timeout) {

        // listen for the $ionicView.enter event:
      $scope.$on('$ionicView.enter', function(e) {});

      var service = {};

      service.GetAll = GetAll;
      service.GetById = GetById;
      service.GetByUsername = GetByUsername;

      //  CUD
      service.Create = Create;
      service.Update = Update;
      service.Delete = Delete;

      return service;

      function GetAll() {
        var deferred = $q.defer();
        deferred.resolve(getUsers());
        return deferred.promise;
      }

      function GetById(id){
        var deferred = $q.defer();
        var filtered = $filter('filter')(getUsers(), { id: id });
        var user = filtered.length ? filtered[0] : null;
        deferred.resolve(user);
        return deferred.promise;
      }

      function GetByUsername(username) {

      }

      function Create(user) {

      }

      function Update(user){

      }

      function Delete(id){

      }

      // private functions

      function getUsers() {
        if(!localStorage.users){
          localStorage.users = JSON.stringify([]);
        }

        return JSON.parse(localStorage.users);
      }

      function setUsers(users) {
        localStorage.users = JSON.stringify(users);
      }

    }

})();
