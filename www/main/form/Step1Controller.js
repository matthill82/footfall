/**
 * Created by matthewhill on 08/09/15.
 */

(function () {

  'use strict';

  angular.module('footFall')
    .controller('Step1Controller', Step1Controller);

  Step1Controller.$inject = ['$scope', '$rootScope', 'localStorageService', '$timeout'];

  function Step1Controller($scope, $rootScope, localStorageService, $timeout) {

      //Contains the filter options
      $scope.filterOptions = {
          categories: [
              {id : 0, age : '18 - 25' },
              {id : 1, age : '25 - 30' },
              {id : 2, age : '30 - 35' },
              {id : 3, age : '35 - 40' },
              {id : 4, age : '45 - 50' },
              {id : 5, age : '55 - 60' },
              {id : 6, age : '60 - 65' },
              {id : 6, age : '65 - 70' }
          ]
      };

      //Mapped to the model to filter
      $scope.filterItem = {
          category: $scope.filterOptions.categories[0]
      };
  }

})();
