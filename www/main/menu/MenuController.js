/**
 * Created by mhill168 on 07/09/15.
 */

(function () {

    'use strict';

    angular.module('footFall')
        .controller('MenuController', MenuController);

    MenuController.$inject = ['$scope', '$ionicPopup', '$filter', '$rootScope', 'localStorageService', '$timeout'];

    function MenuController($scope, $ionicPopup, $filter, $rootScope, localStorageService, $timeout) {

      var inStore = localStorageService.get('customers');
      $rootScope.customers = inStore || [];
      console.log($rootScope.customers);

      $scope.$watch('customers', function () {
        localStorageService.set('customers', $rootScope.customers);
        console.log($rootScope.customers);
      }, true);

        // listen for the $ionicView.enter event:
        $scope.$on('$ionicView.enter', function(e) {
        });

        $scope.date = $filter('date')(new Date(), 'EEE d MMM h:mm', 'GMT');

        // Triggered on a button click, or some other target
        $scope.createUser = function() {

          // An elaborate, custom popup
          var createUserAction = $ionicPopup.show({
            template: '<input type="text" ng-model="customers.name">',
            title: 'Add Customer',
            subTitle: 'Please add customers first name',
            scope: $rootScope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Add</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$rootScope.customers.name) {
                    //don't allow the user to close unless he enters name
                    e.preventDefault();
                  } else {
                    console.log($scope.customers.name);
                    $rootScope.customers.push({id: $rootScope.customers.length + 1, name: $rootScope.customers.name, date: $scope.date });
                    createUserAction.close();
                    $rootScope.customers.name = "";
                  }
                }
              }
            ]
          });
          createUserAction.then(function(res) {
            console.log('Tapped!', res);
          });
        };
    }

})();

