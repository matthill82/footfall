/**
 * Created by mhill168 on 26/06/2015.
 */

(function () {

    'use strict';

    angular.module('glits.storage', [])

        .factory('$localStorage', ['$window', '$rootScope', function ($window, $rootScope) {
            angular.element($window).on('storage', function (event) {
              if(event.key) {
                console.log(event.key);
                $rootScope.$apply();
              }
            });
            return {
                set: function (key, value) {
                    $window.localStorage[key] = value;
                },
                get: function (key, defaultValue) {
                    return $window.localStorage[key] || defaultValue;
                },
                setObject: function (key, value) {
                    $window.localStorage[key] = JSON.stringify(value);
                },
                getObject: function (key) {
                    return JSON.parse($window.localStorage[key] || '{}');
                }
            }
        }]);

})();

